from django.db import models
from django.conf import settings

# Create your models here.


class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title


class RecipeStep(models.Model):
    instruction = models.TextField()
    step_number = models.PositiveSmallIntegerField()
    recipe = models.ForeignKey(
        Recipe,
        on_delete=models.CASCADE,
        related_name="steps"
    )

    def recipe_title(self):
        return self.recipe.title

    class Meta:
        ordering = ["step_number"]


class RecipeIngredients(models.Model):
    amount = models.CharField(max_length=100, null=True)
    food_item = models.CharField(max_length=100, null=True)

    recipe = models.ForeignKey(
        Recipe,
        on_delete=models.CASCADE,
        related_name="ingredients",
        null=True,
    )

    def recipe_title(self):
        return self.recipe.title

    class Meta:
        ordering = ["food_item"]
