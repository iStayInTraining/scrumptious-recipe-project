# Generated by Django 4.2.1 on 2023-05-30 22:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0005_recipeingredients'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='recipeingredients',
            options={'ordering': ['food_item']},
        ),
        migrations.RemoveField(
            model_name='recipeingredients',
            name='instruction',
        ),
        migrations.RemoveField(
            model_name='recipeingredients',
            name='step_number',
        ),
        migrations.AddField(
            model_name='recipeingredients',
            name='amount',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='recipeingredients',
            name='food_item',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='recipeingredients',
            name='recipe',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ingredients', to='recipes.recipe'),
        ),
    ]
